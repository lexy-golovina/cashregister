package com.example;

import com.example.command.CashRegisterControl;
import com.example.command.GiveChangeCommand;
import com.example.command.PutCommand;
import com.example.command.QuitCommand;
import com.example.command.ReadyCommand;
import com.example.command.ShowCommand;
import com.example.command.TakeCommand;

import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {

        int [] coinAmount = {1, 2, 3, 4, 5};
        CashRegister cashRegister = new CashRegister(coinAmount);

        CashRegisterControl cashRegisterControl = new CashRegisterControl();

        cashRegisterControl.setCommand(new ReadyCommand(cashRegister));
        cashRegisterControl.executeCommand();

        Scanner scanner = new Scanner(System.in);
        while (true) {
            String command = scanner.nextLine();
            String[] split = command.split(" ");
            switch (split[0]) {
                case "show":
                    cashRegisterControl.setCommand(new ShowCommand(cashRegister));
                    break;
                case "put":
                    cashRegisterControl.setCommand(new PutCommand(cashRegister, parseIntoIntArray(split)));
                    break;
                case "take":
                    cashRegisterControl.setCommand(new TakeCommand(cashRegister, parseIntoIntArray(split)));
                    break;
                case "change":
                    cashRegisterControl.setCommand(new GiveChangeCommand(cashRegister, parseIntoInt(split[1])));
                    break;
                case "quit":
                    cashRegisterControl.setCommand(new QuitCommand(cashRegister));
                    cashRegisterControl.executeCommand();
                    return;
                default:
                    System.out.println("Wrong command");
            }
            cashRegisterControl.executeCommand();
        }
    }

    private static int [] parseIntoIntArray(String [] items) {
        int [] result = new int[items.length - 1];
        IntStream.range(1, items.length)
                .forEach(i -> result[i - 1] = parseIntoInt(items[i]));
        return result;
    }

    private static int parseIntoInt(String item) {
        try {
            return Integer.parseInt(item);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(item + " is not an int");
        }
    }
}
