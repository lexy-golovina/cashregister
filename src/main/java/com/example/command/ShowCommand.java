package com.example.command;

import com.example.CashRegister;

public class ShowCommand implements Command {

    private CashRegister cashRegister;

    public ShowCommand(CashRegister cashRegister) {
        this.cashRegister = cashRegister;
    }

    @Override
    public void execute() {
        System.out.println(cashRegister.show());
    }
}
