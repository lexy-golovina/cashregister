package com.example.command;

@FunctionalInterface
public interface Command {
    void execute();
}
