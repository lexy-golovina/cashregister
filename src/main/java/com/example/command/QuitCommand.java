package com.example.command;

import com.example.CashRegister;

public class QuitCommand implements Command {

    private CashRegister cashRegister;

    public QuitCommand(CashRegister cashRegister) {
        this.cashRegister = cashRegister;
    }

    @Override
    public void execute() {
        System.out.println(cashRegister.quit());
    }
}
