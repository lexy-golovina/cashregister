package com.example.command;

import com.example.CashRegister;

public class ReadyCommand implements Command {

    private CashRegister cashRegister;

    public ReadyCommand(CashRegister cashRegister) {
        this.cashRegister = cashRegister;
    }

    @Override
    public void execute() {
        System.out.println(cashRegister.ready());
    }
}
