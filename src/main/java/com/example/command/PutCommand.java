package com.example.command;

import com.example.CashRegister;

public class PutCommand implements Command {

    private CashRegister cashRegister;
    private int [] bills;

    public PutCommand(CashRegister cashRegister, int[] bills) {
        this.cashRegister = cashRegister;
        this.bills = bills;
    }

    @Override
    public void execute() {
        System.out.println(cashRegister.put(bills));
    }
}
