package com.example.command;

import com.example.CashRegister;

public class GiveChangeCommand implements Command {

    private CashRegister cashRegister;
    private int money;

    public GiveChangeCommand(CashRegister cashRegister, int money) {
        this.cashRegister = cashRegister;
        this.money = money;
    }

    @Override
    public void execute() {
        System.out.println(cashRegister.change(money));
    }
}
