package com.example;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CashRegister {

    private static final int [] DENOMINATION = {20, 10, 5, 2, 1};
    private static final String NO_CHANGE = "Sorry";
    private static final String READY_FOR_INPUT = "Ready";
    private static final String QUIT_PROGRAM = "Bye";
    private int [] coinAmounts;

    public CashRegister(int [] coinAmounts) {
        this.coinAmounts = coinAmounts;
    }

    public String show() {
        inputCheck();

        int total = IntStream.range(0, DENOMINATION.length)
                .map(i -> DENOMINATION[i] * coinAmounts[i])
                .sum();

        return Arrays.stream(coinAmounts)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(" ", "$" + total + " ", ""));
    }

    public String put(int [] bills) {
        inputCheck();

        IntStream.range(0, DENOMINATION.length)
                .forEach(i -> coinAmounts[i] += bills[i]);
        return show();
    }

    public String take(int [] bills) {
        inputCheck();

        IntStream.range(0, DENOMINATION.length).forEach(i -> {
            if (bills[i] > coinAmounts[i]) {
                throw new IllegalArgumentException("Cannot withdraw");
            }
            coinAmounts[i] -= bills[i];
        });

        return show();
    }

    private void inputCheck() {
        if (coinAmounts == null || coinAmounts.length != DENOMINATION.length) {
            throw new IllegalArgumentException("Coins amount is not equal to denomination amount");
        }
    }

    public String change(int money) {
        Optional<int[]> res = findSolution(money, Arrays.copyOf(coinAmounts, coinAmounts.length));
        if (res.isPresent()) {
            return processResult(res.get());
        }
        return NO_CHANGE;
    }

    private String processResult(int[] res) {
        String coinsAfterChange = IntStream.range(0, DENOMINATION.length)
                .mapToObj(i -> String.valueOf(coinAmounts[i] - res[i]))
                .collect(Collectors.joining(" "));

        coinAmounts = res;
        return coinsAfterChange;
    }

    private Optional<int[]> findSolution(int total, int [] coins) {
        if (total == 0) {
            return Optional.of(coins);
        }
        if (total < 0) {
            return Optional.empty();
        }

        for (int i = 0; i < coins.length; i++) {
            if (DENOMINATION[i] <= total && coins[i] > 0) {
                --coins[i];
                Optional<int[]> res = findSolution(total - DENOMINATION[i], coins);
                if (res.isPresent()) {
                    return res;
                }
                ++coins[i];
            }
        }

        return Optional.empty();
    }

    public String ready() {
        return READY_FOR_INPUT;
    }

    public String quit() {
        return QUIT_PROGRAM;
    }
}
