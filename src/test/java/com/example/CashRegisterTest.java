package com.example;

import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.assertThat;

public class CashRegisterTest {

    private CashRegister cashRegister;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        int [] coinAmount = {1, 2, 3, 4, 5};
        cashRegister = new CashRegister(coinAmount);
    }

    @Test
    public void shouldThrowAnExceptionWhenInputIsEmpty() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Coins amount is not equal to denomination amount");

        cashRegister = new CashRegister(null);
        cashRegister.show();
    }

    @Test
    public void shouldThrowAnExceptionWhenInputLength() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Coins amount is not equal to denomination amount");

        cashRegister = new CashRegister(new int[] {1});
        cashRegister.show();
    }

    @Test
    public void shouldReturnCorrectSumAndCoins() {
        String result = cashRegister.show();
        assertThat(result).isEqualTo("$68 1 2 3 4 5");
    }

    @Test
    public void shouldReturnCorrectSumAndCoinsAfterPut() {
        String result = cashRegister.put(new int[] {1,2,3,0,5});
        assertThat(result).isEqualTo("$128 2 4 6 4 10");
    }

    @Test
    public void shouldThrowExceptionWhenCannotWithdraw() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Cannot withdraw");

        cashRegister.take(new int[]{1, 4, 3, 0, 10});
    }

    @Test
    public void shouldReturnCorrectSumAndCoinsAfterTake() {
        cashRegister.put(new int[] {1,2,3,0,5});
        String result = cashRegister.take(new int[]{1, 4, 3, 0, 10});
        assertThat(result).isEqualTo("$43 1 0 3 4 0");
    }

    @Test
    public void shouldReturnCorrectChange() {
        cashRegister.put(new int[] {1,2,3,0,5});
        cashRegister.take(new int[]{1, 4, 3, 0, 10});
        String result = cashRegister.change(11);
        String currentState = cashRegister.show();

        SoftAssertions.assertSoftly(softAssertions -> {
            assertThat(result).isEqualTo("0 0 1 3 0");
            assertThat(currentState).isEqualTo("$32 1 0 2 1 0");
        });
    }

    @Test
    public void shouldNotReturnChange() {
        cashRegister.put(new int[] {1,2,3,0,5});
        cashRegister.take(new int[]{1, 4, 3, 0, 10});
        cashRegister.change(11);
        String result = cashRegister.change(14);
        assertThat(result).isEqualTo("Sorry");
    }
}